# Hilltop API

Welcome to Hilltop API, a beautiful view over the Cardano Blockchain's data

# How to Deploy

The hilltop-api is designed to be deployed on [dandelion.link](https://dandelion.link)

You can deploy it using helm as follows:

```bash
helm upgrade --install hilltop-api \
    -n dl-mainnet-v10-0-1 \
    -f deploy/hilltop-api/values-mainnet.yaml \
    deploy/hilltop-api
```

Of course namespace and values file need to be configured accordingly to which cardano network you're deploying to
