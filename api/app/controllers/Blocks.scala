package controllers

import dao.BlockDao
import io.gimbalabs.hilltop.api.v0.models.json._
import io.gimbalabs.hilltop.api.v0.models.{Block, DayBlock, GenericError}
import org.joda.time.LocalDate
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import java.util.concurrent.Executors
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class Blocks @Inject()(blockDao: BlockDao) extends InjectedController {

  private val executors = Executors.newCachedThreadPool()

  private implicit val ec = ExecutionContext.fromExecutor(executors)

  def getByBlockNumber(blockNumber: Long) = Action.async {
    Future {

      val block = blockDao
        .findByBlockNumber(blockNumber)
        .flatMap(toModelBlock)
      block
        .fold(NotFound(Json.toJson(GenericError("Block Number could not be found"))))(block => Ok(Json.toJson(block)))

    }
  }

  def getBlocksInDay(date: LocalDate) = Action.async {
    Future {
      val (firstOpt, lastOpt) = blockDao.findBlockRangeInDay(date)

      val dayBlock = for {
        first <- firstOpt
        last <- lastOpt
        firstBlock <- toModelBlock(first)
        lastBlock <- toModelBlock(last)
      } yield DayBlock(firstBlock, lastBlock)

      dayBlock
        .fold(NotFound(Json.toJson(GenericError("Block Number could not be found"))))(dayBlock => Ok(Json.toJson(dayBlock)))

    }
  }

  private def toModelBlock(block: dao.Block): Option[Block] = for {
    blockNumber <- block.number
    epochNumber <- block.epochNumber
    slotNumber <- block.slotNumber
    epochSlotNumber <- block.epochSlotNumber
  } yield Block(blockNumber,
    epochNumber,
    slotNumber,
    epochSlotNumber,
    block.size,
    block.transactionCount,
    block.time
  )
}