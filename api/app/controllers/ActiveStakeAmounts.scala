package controllers

import dao.{ActiveStakeDao, EpochDao, PoolHashDao}
import io.gimbalabs.hilltop.api.v0.models.json._
import io.gimbalabs.hilltop.api.v0.models.{ActiveStakeAmount, AddressStakeAmount}
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import javax.inject.{Inject, Singleton}

@Singleton
class ActiveStakeAmounts @Inject()(poolHashDao: PoolHashDao,
                                   epochDao: EpochDao,
                                   activeStakeDao: ActiveStakeDao) extends InjectedController {

  def get(epochNumber: Option[Int]) = Action {
    val activeStakeAmount = activeStakeDao.activeStakeByEpochNumber(epochNumber.getOrElse(epochDao.currentEpoch.no))
    Ok(Json.toJson(ActiveStakeAmount(activeStakeAmount)))
  }

  def getByPoolBech32Id(poolBech32Id: String, epochNumber: Option[Int]) = Action {
    poolHashDao
      .findByPoolId(poolBech32Id) match {
      case Some(poolHash) =>
        val activeStakeAmount = activeStakeDao.activeStakeByEpochNumber(epochNumber.getOrElse(epochDao.currentEpoch.no), poolHash.id)
        Ok(Json.toJson(ActiveStakeAmount(activeStakeAmount)))
      case None => NotFound(s"Pool could not be found (Bech32: $poolBech32Id)")
    }
  }

  def getAddressesByPoolBech32Id(poolBech32Id: String, epochNumber: Int) = Action {
    val addressesAmounts = activeStakeDao
      .findByPoolIdAndEpoch(poolBech32Id, epochNumber)
      .map(epochStake => AddressStakeAmount(epochStake.stakeAddressId, epochStake.amount))
    Ok(Json.toJson(addressesAmounts))
  }

}