package controllers

import dao._
import io.gimbalabs.hilltop.api.v0.models.Reward
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

@Singleton
class Rewards @Inject()(rewardsDao: RewardsDao) extends InjectedController {

  def getByStakingAddress(stakingAddress: String, epochFrom: Option[Int], epochTo: Option[Int]) = Action {
    val rewards = rewardsDao
      .findAll(stakingAddress, epochFrom, epochTo)
      .map(reward => Reward(stakingAddress,
        reward.epoch,
        reward.amount,
        new String(Hex.encodeHex(reward.poolId))
      ))
    Ok(Json.toJson(rewards))
  }

}