package controllers

import io.gimbalabs.hilltop.api.v0.models.DelegatorsCount
import io.gimbalabs.hilltop.api.v0.models.json._
import dao.{DelegatorsDao, PoolHashDao}
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import javax.inject.{Inject, Singleton}

@Singleton
class DelegatorsCounts @Inject()(poolHashDao: PoolHashDao,
                                 delegatorsDao: DelegatorsDao) extends InjectedController {

  def getByPoolBech32Id(poolBech32Id: String) = Action {
    poolHashDao
      .findByPoolId(poolBech32Id) match {
      case Some(poolHash) =>
        val delegatorsCount = delegatorsDao.countByPoolId(poolHash.id)
        Ok(Json.toJson(DelegatorsCount(delegatorsCount)))
      case None => NotFound(s"Pool could not be found (Bech32: $poolBech32Id)")

    }
  }

}