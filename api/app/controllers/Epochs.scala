package controllers

import io.gimbalabs.hilltop.api.v0.models.Epoch
import io.gimbalabs.hilltop.api.v0.models.json._
import dao.EpochDao
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import javax.inject.{Inject, Singleton}

@Singleton
class Epochs @Inject()(epochDao: EpochDao) extends InjectedController {

  private def toModelEpoch(dbEpoch: db.model.Epoch) = Epoch(
    dbEpoch.outSum,
    dbEpoch.fees,
    dbEpoch.txCount,
    dbEpoch.blkCount,
    dbEpoch.no,
    dbEpoch.startTime,
    dbEpoch.endTime
  )

  def getCurrent() = Action {
    val currentEpoch = epochDao.currentEpoch
    Ok(Json.toJson(toModelEpoch(currentEpoch)))
  }

  def getByEpochNumber(epochNumber: Int) = Action {
    val epochOpt = epochDao.findByEpochNumber(epochNumber)
    epochOpt match {
      case Some(epoch) => Ok(Json.toJson(toModelEpoch(epoch)))
      case None => NotFound(s"Epoch $epochNumber could not be found")
    }

  }

}