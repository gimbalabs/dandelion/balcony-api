package controllers

import dao._
import io.gimbalabs.hilltop.api.v0.models.json._
import io.gimbalabs.hilltop.api.v0.models.{Delegation, GenericError, StakepoolDelegation}
import play.api.Logging
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex
import services.StakePoolService

import javax.inject.{Inject, Singleton}

@Singleton
class Delegations @Inject()(stakePoolService: StakePoolService,
                            delegatorsDao: DelegatorsDao,
                            ledgerDao: LedgerDao) extends InjectedController with Logging {

  def getStakepoolByStakingAddress(stakingAddress: String) = Action {

    def toString(bytes: Array[Byte]) = new String(Hex.encodeHex(bytes))

    val stakepoolDelegations = delegatorsDao
      .findDelegation(stakingAddress)
      .flatMap { stakeAddressDelegation =>
        val stakePoolId = toString(stakeAddressDelegation.poolId)
        logger.info(s"stakePoolId: $stakePoolId")
        val stakePoolOpt = stakePoolService
          .getActiveStakepools
          .find(_.id == stakePoolId)
          .orElse(stakePoolService
            .getRetiredStakepools
            .find(_.id == stakePoolId))
        if (stakePoolOpt.isEmpty) {
          logger.info(s"Could not find stakepool w/ id: $stakePoolId")
        }
        stakePoolOpt.map(stakePool => StakepoolDelegation(stakePool, stakeAddressDelegation.activeEpochNumber))
      }

    Ok(Json.toJson(Delegation(stakingAddress, stakepoolDelegations)))
  }

  def getSafePaymentByStakingAddress(stakingAddress: String) = Action {
    ledgerDao
      .getSafePaymentAddress(stakingAddress) match {
      case Some(paymentAddress) => Ok(Json.toJson(paymentAddress))
      case None => NotFound(Json.toJson(GenericError(s"No payment address found for staking address: $stakingAddress")))
    }
  }

}