package controllers

import akka.stream.Materializer
import dao._
import io.gimbalabs.hilltop.api.v0.models
import io.gimbalabs.hilltop.api.v0.models.GenericError
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.Logging
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.InjectedController

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class StakepoolMetadata @Inject()(poolMetadataRefDao: PoolMetadataRefDao, wsClient: WSClient)(implicit executionContext: ExecutionContext, materializer: Materializer) extends InjectedController with Logging {

  def getByPoolIdBech32(poolIdBech32: String) = Action.async {
    poolMetadataRefDao
      .findByPoolBechId(poolIdBech32)
      .map { metadata =>
        wsClient
          .url(metadata.url)
          .withHttpHeaders("Accept" -> "application/json")
          .get()
          .map(_.json.as[models.StakepoolMetadata])
          .map(stakepoolMetadata => Ok(Json.toJson(stakepoolMetadata)))
      }.getOrElse(Future.successful(NotFound(Json.toJson(GenericError("")))))
  }
}