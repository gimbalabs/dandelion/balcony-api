package controllers

import dao.NativeAssetsDao
import io.gimbalabs.hilltop.api.v0.models._
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

@Singleton
class MultiAssets @Inject()(nativeAssetsDao: NativeAssetsDao) extends InjectedController {

  def getAssetNamesByPolicyId(policyId: String) = Action {
    val assetNames = nativeAssetsDao
      .findAssetNameForPolicy(policyId)
      .map(nameBytes => new String(nameBytes))
    Ok(Json.toJson(assetNames))
  }


}