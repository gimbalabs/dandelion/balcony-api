package controllers

import io.gimbalabs.hilltop.api.v0.models.Healthcheck
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import javax.inject.Singleton

@Singleton
class Healthchecks extends InjectedController {

  def get() = Action {
    Ok(Json.toJson(Healthcheck("healthy")))
  }

}
