package controllers

import dao._
import io.gimbalabs.hilltop.api.v0.models.GenericError
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.Logging
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex
import services.StakePoolService

import javax.inject.{Inject, Singleton}

@Singleton
class Stakepools @Inject()(stakePoolsDao: StakePoolsDao,
                           epochDao: EpochDao,
                           poolUpdateDao: PoolUpdateDao,
                           stakePoolService: StakePoolService) extends InjectedController with Logging {

  def get = Action {
    Ok(Json.toJson(stakePoolService.getActiveStakepools))
  }

  def getEstablishedEpochByPoolId(poolId: String) = Action {
    poolUpdateDao
      .activeStakeByEpochNumber(poolId)
      .fold(NotFound(Json.toJson(GenericError(s"Pool with id $poolId could not be found")))) { epoch =>
        Ok(Json.toJson(epoch))
      }
  }

  def getLiveStakeByPoolId(poolId: String) = Action {
    stakePoolsDao
      .liveStake(poolId)
      .fold(NotFound(Json.toJson(GenericError(s"Pool with id $poolId could not be found")))) { liveStake =>
        Ok(Json.toJson(liveStake))
      }
  }

  def getBlocksByPoolId(poolId: String, epoch: Option[Int]) = Action {

    val epochNumber = epoch.getOrElse(epochDao.currentEpoch.no)

    stakePoolsDao
      .mintedBlocksInEpoch(poolId, epochNumber)
      .fold(NotFound(Json.toJson(GenericError(s"Pool with id $poolId could not be found")))) { liveStake =>
        Ok(Json.toJson(liveStake))
      }
  }

  def getVrfVkeyByVrfVkeyHash(vrfVkeyHash: String) = Action {
    (for {
      bech32PoolHash <- poolUpdateDao
        .findByVrfVkeyHash(vrfVkeyHash)
      bech32Pool = new String(Hex.encodeHex(bech32PoolHash))
      pool <- stakePoolService.getActiveStakepools.find(_.poolIdBech32 == bech32Pool)
    } yield {
      Ok(Json.toJson(pool))
    }).getOrElse(NotFound("Pool or vrf key not found."))
  }

}