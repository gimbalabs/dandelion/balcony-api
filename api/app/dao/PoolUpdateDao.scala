package dao

import anorm.Macro.ColumnNaming
import anorm._
import play.api.db.Database
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

case class PoolUpdate(epochNo: Int,
                      blockNumber: Long,
                      txHash: Array[Byte],
                      tickerName: Option[String],
                      hashRaw: Array[Byte],
                      certIndex: Int,
                      vrfKeyHash: Array[Byte],
                      pledge: Long,
                      rewardAddr: Array[Byte],
                      activeEpochNo: Long,
                      metaId: Option[Long],
                      margin: Double,
                      fixedCost: Long,
                      registeredTxId: Long)

@Singleton
class PoolUpdateDao @Inject()(database: Database) {

  // Group/Count number of time pools have changed ticker
  // select max(pool_id), count(distinct(ticker_name)) from pool_offline_data group by pool_id order by count desc;

  private val parser = Macro.namedParser[PoolUpdate](ColumnNaming.SnakeCase)

  def findAll(minBlockNumber: Option[Long] = None, limit: Option[Long] = Some(1000L)) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT b.epoch_no, b.block_no AS block_number, tx.hash AS tx_hash, pod.ticker_name, ph.hash_raw, pu.*
        | FROM pool_update pu
        |   INNER JOIN tx
        |     ON pu.registered_tx_id = tx.id
        |   INNER JOIN block b
        |     ON tx.block_id = b.id
        |   INNER JOIN pool_hash ph
        |     ON pu.hash_id = ph.id
        |   LEFT OUTER JOIN pool_offline_data pod
        |     on pu.meta_id = pod.pmr_id
        | WHERE b.block_no >= {blockNumber}
        | ORDER BY pu.id
        | LIMIT {limit}
        """.stripMargin)
      .on("blockNumber" -> minBlockNumber.getOrElse(0L),
        "limit" -> limit.getOrElse(Long.MaxValue))
      .as(parser.*)
  }

  def findAllByPoolBech32Id(poolBech32Id: String, minBlockNumber: Option[Long] = None, limit: Option[Long] = Some(1000L)) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT b.epoch_no, b.block_no AS block_number, tx.hash AS tx_hash, pod.ticker_name, ph.hash_raw, pu.*
        | FROM pool_update pu
        |   INNER JOIN tx
        |     ON pu.registered_tx_id = tx.id
        |   INNER JOIN block b
        |     ON tx.block_id = b.id
        |   INNER JOIN pool_hash ph
        |     ON pu.hash_id = ph.id
        |   LEFT OUTER JOIN pool_offline_data pod
        |     on pu.meta_id = pod.pmr_id
        | WHERE b.block_no >= {blockNumber}
        |   AND ph.hash_raw = {hashRaw}
        | ORDER BY pu.id
        | LIMIT {limit}
        """.stripMargin)
      .on("hashRaw" -> Hex.decodeHex(poolBech32Id.toCharArray),
        "blockNumber" -> minBlockNumber.getOrElse(0L),
        "limit" -> limit.getOrElse(Long.MaxValue))
      .as(parser.*)
  }

  //  https://dba.stackexchange.com/questions/199556/postgresql-select-row-with-minimum-value-by-certain-column
  def activeStakeByEpochNumber(stakePoolBech32Id: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT DISTINCT ON (hash_id) active_epoch_no
        | FROM pool_update pu
        | INNER JOIN pool_hash ph
        |   ON pu.hash_id = ph.id
        | WHERE
        |   hash_raw = {hashRaw} ORDER BY hash_id, registered_tx_id
        """.stripMargin)
      .on("hashRaw" -> Hex.decodeHex(stakePoolBech32Id.toCharArray))
      .as(SqlParser.scalar[Int].singleOpt)
      .map(epoch => epoch - 2)
  }

  def findByVrfVkeyHash(vrfVkeyHash: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT DISTINCT ON (pu.hash_id) ph.hash_raw
        | FROM pool_update pu
        | INNER JOIN pool_hash ph
        |   ON pu.hash_id = ph.id
        | WHERE
        |   pu.vrf_key_hash = {vrfVkeyHash} ORDER BY pu.hash_id, pu.registered_tx_id
        """.stripMargin)
      .on("vrfVkeyHash" -> Hex.decodeHex(vrfVkeyHash.toCharArray))
      .as(SqlParser.scalar[Array[Byte]].singleOpt)
  }


}
