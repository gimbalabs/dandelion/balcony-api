package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL, SqlParser}
import com.google.inject.{Inject, Singleton}
import play.api.db.Database
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

case class Reward(epoch: Int, amount: Long, poolId: Array[Byte])

@Singleton
class RewardsDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[Reward](ColumnNaming.SnakeCase)

  def findAll(stakingAddress: String, epochFrom: Option[Int], epochTo: Option[Int]) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT r.earned_epoch epoch, r.amount, ph.hash_raw as pool_id
        | FROM reward r
        | INNER JOIN stake_address sa
        |   ON sa.id = r.addr_id
        | INNER JOIN pool_hash ph
        |   ON r.pool_id = ph.id
        | WHERE sa.view = {stakingAddress}
        | AND spendable_epoch BETWEEN {epochFrom} AND {epochTo};
        """.stripMargin)
      .on("stakingAddress" -> stakingAddress,
        "epochFrom" -> epochFrom.getOrElse(0),
        "epochTo" -> epochTo.getOrElse(Int.MaxValue))
      .as(parser.*)
  }
}
