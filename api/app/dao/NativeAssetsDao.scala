package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL}
import com.google.inject.{Inject, Singleton}
import play.api.db.Database
import play.api.libs.json._
import anorm._
import org.joda.time.DateTime
import play.shaded.oauth.org.apache.commons.codec.binary.Hex
import postgresql._

case class Assets(policyId: String,
                  tokenName: String,
                  txId: String,
                  tid: Long,
                  metadataJson: JsValue,
                  invalidBefore: Option[Long],
                  invalidHereafter: Option[Long],
                  blockNo: Long,
                  epochNo: Int,
                  epochSlotNo: Long,
                  mintId: Long,
                  quantity: Int)

case class MintingTransaction(policy: Array[Byte], name: Array[Byte], blockNo: Long, blockTime: DateTime, fingerprint: String)

case class AssetDetails(policy: Array[Byte], name: Array[Byte], mintedQuantity: Long, metadataKey: Option[Long], metadataJson: Option[JsValue], mintedAt: DateTime, mintedAtBlock: Long, txHash: Array[Byte], fingerprint: String)

@Singleton
class NativeAssetsDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[Assets](ColumnNaming.SnakeCase)

  private val mintingTxParser = Macro.namedParser[MintingTransaction](ColumnNaming.SnakeCase)

  private val assetDetailsParser = Macro.namedParser[AssetDetails](ColumnNaming.SnakeCase)

  // This query is sourced from: https://github.com/wutzebaer/cardano-tools/blob/main/src/main/java/de/peterspace/cardanotools/dbsync/CardanoDbSyncClient.java#L65
  // And used under the terms of the MIT Licence specified here: https://github.com/wutzebaer/cardano-tools/blob/main/LICENSE
  def findByStakeAddress(stakeAddress: String): List[Assets] = database.withConnection { implicit c =>
    SQL(
      """
        | WITH
        |   stake_address_id as (
        |	    SELECT to2.stake_address_id
        |	    FROM tx_out to2
        |	    WHERE to2.address = {stakeAddress}
        |	      UNION ALL
        |	      SELECT sa.id
        |	      FROM stake_address sa
        |	      WHERE sa."view" = {stakeAddress}
        |	      LIMIT 1
        |   ),
        |		owned_tokens as (
        |	    SELECT ma.policy "policy", ma.name "name", quantity quantity, to2.id txId, mto.ident ident
        |	    FROM utxo_view uv
        |	    join tx_out to2 on to2.tx_id = uv.tx_id and to2."index" = uv."index"
        |	    join ma_tx_out mto on mto.tx_out_id = to2.id
        |     join multi_asset ma on ma.id = mto.ident
        |	    where uv.stake_address_id = (select * from stake_address_id)
        |		)
        |		SELECT
        |			encode(ot.policy::bytea, 'hex') policy_id,
        |			encode(ot.name::bytea, 'escape') token_name,
        |			max(ot.quantity) quantity,
        |			max(encode(t.hash ::bytea, 'hex')) tx_id,
        |			jsonb_agg(tm.json)->0 AS metadata_json,
        |			max(t.invalid_before) invalid_before,
        |			max(t.invalid_hereafter) invalid_hereafter,
        |			max(b.block_no) block_no,
        |			max(b.epoch_no) epoch_no,
        |			max(b.epoch_slot_no) epoch_slot_no, 
        |			max(t.id) tid, 
        |			max(mtm.id) mint_id
        |			from owned_tokens ot
        |			join ma_tx_mint mtm on mtm.ident = ot.ident
        |			join tx t on t.id = mtm.tx_id 
        |			left join tx_metadata tm on tm.tx_id = t.id 
        |			join block b on b.id = t.block_id
        |			group by ot.policy, ot.name, ot.ident
        |			order by (select min(id) from ma_tx_mint sorter where sorter.ident = ot.ident) desc
        |""".stripMargin
    )
      .on("stakeAddress" -> stakeAddress)
      .as(parser.*)
  }

  def findAllMintingTransactions(blockNumber: Option[Long] = None, limit: Option[Long] = Some(1000L)) = database.withConnection { implicit c =>
    SQL(
      s"""
         | SELECT ma.policy, ma.name, b.block_no, b.time at time zone 'UTC' as block_time, ma.fingerprint
         | FROM ma_tx_mint mtm
         | INNER JOIN multi_asset ma on ma.id = mtm.ident
         | INNER JOIN tx t on t.id = mtm.tx_id
         | INNER JOIN block b on t.block_id = b.id
         | WHERE b.block_no >= {blockNumber}
         | ORDER BY b.block_no ASC
         | LIMIT {limit}
         |""".stripMargin)
      .on("blockNumber" -> blockNumber.getOrElse(0L), "limit" -> limit.getOrElse(Long.MaxValue))
      .as(mintingTxParser.*)
  }

  def findAssetNameForPolicy(policyId: String) = database.withConnection { implicit c =>
    SQL(
      s"""
         | SELECT distinct(ma.name)
         | FROM ma_tx_mint mtm
         | INNER JOIN multi_asset ma on ma.id = mtm.ident
         | WHERE ma.policy = {policyId}
         |""".stripMargin)
      .on("policyId" -> Hex.decodeHex(policyId.toCharArray))
      .as(SqlParser.scalar[Array[Byte]].*)
  }

  def findAllMintingTransactions(policyId: String) = database.withConnection { implicit c =>
    SQL(
      s"""
         | SELECT ma.policy, ma.name, max(b.block_no) as block_no
         | FROM ma_tx_mint mtm
         | INNER JOIN multi_asset ma on ma.id = mtm.ident
         | INNER JOIN tx t on t.id = mtm.tx_id
         | INNER JOIN block b on t.block_id = b.id
         | WHERE ma.policy = {policyId}
         | GROUP BY ma.policy, ma.name
         |""".stripMargin)
      .on("policyId" -> Hex.decodeHex(policyId.toCharArray))
      .as(mintingTxParser.*)
  }

  def findAllMintingTransactionsDetails(policyId: String, assetNames: Seq[String]) = database.withConnection { implicit c =>

    SQL(
      """
        | SELECT
        |	  policy,
        |		name,
        |	  mtm.quantity AS minted_quantity,
        |		tm.key::bigint AS metadata_key,
        |		tm.json AS metadata_json,
        |   b.block_no as minted_at_block,
        |   b.time at time zone 'UTC' as minted_at,
        |   t.hash as tx_hash,
        |   ma.fingerprint
        |		FROM ma_tx_mint mtm
        |   INNER JOIN multi_asset ma on ma.id = mtm.ident
        |		INNER JOIN tx t
        |     ON t.id = mtm.tx_id
        |		LEFT OUTER JOIN tx_metadata tm
        |     ON tm.tx_id = t.id
        |		INNER JOIN block b
        |     ON b.id = t.block_id
        | WHERE ma.policy = {policyId}
        |   AND ma.name in ({assetNames})
        |""".stripMargin)

      .on("policyId" -> Hex.decodeHex(policyId.toCharArray), "assetNames" -> assetNames.map(_.getBytes))
      .as(assetDetailsParser.*)
  }

  def findNftMintingTransactionsDetailsAtBlock(blockNumberFrom: Long, blockNumberTo: Long) = database.withConnection { implicit c =>

    SQL(
      """
        | SELECT
        |   ma.policy,
        |   ma.name,
        |   mtm.quantity AS minted_quantity,
        |	  tm.key::bigint AS metadata_key,
        |	  tm.json AS metadata_json,
        |   b.block_no as minted_at_block,
        |   b.time at time zone 'UTC' as minted_at,
        |   t.hash as tx_hash,
        |   ma.fingerprint
        | FROM ma_tx_mint mtm
        | INNER JOIN multi_asset ma
        |   ON ma.id = mtm.ident
        | INNER JOIN tx t
        |   ON t.id = mtm.tx_id
        | LEFT OUTER JOIN tx_metadata tm
        |   ON tm.tx_id = t.id
        | INNER JOIN block b
        |   ON b.id = t.block_id
        | WHERE b.block_no BETWEEN {blockNumberFrom} and {blockNumberTo}
        |   AND tm.key = 721;
        |""".stripMargin)

      .on("blockNumberFrom" -> blockNumberFrom, "blockNumberTo" -> blockNumberTo)
      .as(assetDetailsParser.*)
  }

}
