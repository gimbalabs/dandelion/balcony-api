package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL, SqlParser, _}
import com.google.inject.{Inject, Singleton}
import play.api.db.Database
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

case class PoolHash(id: Long, hashRaw: Array[Byte], view: String)

@Singleton
class PoolHashDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[PoolHash](ColumnNaming.SnakeCase)

  def findById(id: Long): Option[PoolHash] = database.withConnection { implicit c =>
    SQL("SELECT * FROM pool_hash WHERE id = {id}")
      .on("id" -> id)
      .as(parser.singleOpt)
  }


  def findByPoolId(poolId: String) = database.withConnection { implicit c =>
    SQL("SELECT * FROM pool_hash WHERE view = {poolBech32}")
      .on("poolBech32" -> poolId)
      .as(parser.singleOpt)
  }

  def findByHashRaw(hash: String): Option[PoolHash] = database.withConnection { implicit c =>
    SQL("SELECT * FROM pool_hash WHERE hash_raw = {hashRaw}")
      .on("hashRaw" -> Hex.decodeHex(hash.toCharArray))
      .as(parser.singleOpt)
  }

}
