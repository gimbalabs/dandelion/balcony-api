package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL}
import com.google.inject.{Inject, Singleton}
import db.model.EpochParam
import play.api.db.Database

@Singleton
class EpochParamDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[EpochParam](ColumnNaming.SnakeCase)

  def findByEpochNumber(epochNumber: Int) = database.withConnection { implicit c =>
    SQL("SELECT * FROM epoch_param WHERE epoch_no = {epochNumber}")
      .on("epochNumber" -> epochNumber)
      .as(parser.singleOpt)
  }

}
