package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL, SqlParser}
import com.google.inject.{Inject, Singleton}
import play.api.db.Database
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

case class EpochStake(stakeAddressId: String, amount: Long)

@Singleton
class ActiveStakeDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[EpochStake](ColumnNaming.SnakeCase)

  // This should be cached for epoch < current epoch as it can't change and it will do a full scan table
  // epoch is not indexed :sad_panda:
  def activeStakeByEpochNumber(epochNumber: Int) = database.withConnection { implicit c =>
    SQL("SELECT SUM (amount) FROM epoch_stake WHERE epoch_no = {epochNumber}")
      .on("epochNumber" -> epochNumber)
      .as(SqlParser.scalar[Long].singleOpt)
      .getOrElse(0L)
  }

  def activeStakeByEpochNumber(epochNumber: Int, poolId: Long) = database.withConnection { implicit c =>
    SQL("SELECT SUM (amount) FROM epoch_stake WHERE epoch_no = {epochNumber} and pool_id = {poolId}")
      .on("epochNumber" -> epochNumber, "poolId" -> poolId)
      .as(SqlParser.scalar[Long].singleOpt)
      .getOrElse(0L)
  }

  def findByPoolIdAndEpoch(poolIdBech32: String, epochNumber: Long) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT sa.view as stake_address_id, es.amount
        | FROM epoch_stake es
        |  INNER JOIN pool_hash ph ON es.pool_id = ph.id
        |  INNER JOIN stake_address sa ON es.addr_id = sa.id
        | WHERE ph.hash_raw = {poolIdBech32} and es.epoch_no = {epochNumber}
        |""".stripMargin)
      .on("poolIdBech32" -> Hex.decodeHex(poolIdBech32.toCharArray), "epochNumber" -> epochNumber)
      .as(parser.*)
  }

}
