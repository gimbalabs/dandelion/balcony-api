package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL, SqlParser}
import com.google.inject.{Inject, Singleton}
import play.api.db.Database

case class StakeAddressDelegation(poolId: Array[Byte], activeEpochNumber: Int)

@Singleton
class DelegatorsDao @Inject()(database: Database) {

  private val parserStakeAddressDelegation = Macro.namedParser[StakeAddressDelegation](ColumnNaming.SnakeCase)

  def countByPoolId(poolId: Long) = database.withConnection { implicit c =>
    SQL(
      """ SELECT COUNT(1)
        | FROM delegation d
        | WHERE pool_hash_id = {poolId}
        |   AND NOT EXISTS (SELECT TRUE FROM delegation d2 WHERE d2.addr_id = d.addr_id AND d2.id > d.id)
        |   AND NOT EXISTS (SELECT TRUE FROM stake_deregistration sd WHERE sd.addr_id=d.addr_id AND sd.tx_id > d.tx_id)
        |""".stripMargin)
      .on("poolId" -> poolId)
      .as(SqlParser.scalar[Int].singleOpt)
      .getOrElse(0)
  }

  def findDelegation(stakingAddressId: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT DISTINCT ON (d.active_epoch_no) d.active_epoch_no as active_epoch_number, ph.hash_raw as pool_id, slot_no
        | FROM delegation d
        | INNER JOIN pool_hash ph
        |   ON d.pool_hash_id = ph.id
        | INNER JOIN stake_address sa
        |   ON d.addr_id = sa.id
        | WHERE sa.view = {stakingAddressId}
        | ORDER BY d.active_epoch_no DESC, slot_no DESC;
        |""".stripMargin)
      .on("stakingAddressId" -> stakingAddressId)
      .as(parserStakeAddressDelegation.*)
  }

}
