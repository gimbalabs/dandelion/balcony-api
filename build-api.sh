#!/usr/bin/env bash

set -x

sbt "project api" stage

VERSION=$(git describe --tags)

DOCKER_IMAGE="gimbalabs/hilltop-api:${VERSION}"

echo "Building image: ${DOCKER_IMAGE}"

OS_ARCH=$(uname -m)

if [[ $OS_ARCH == "arm64" || $OS_ARCH == "aarch64" ]];
then
  echo "OS arch: arm64 / aarch64 detected"
  docker buildx use x86
  docker buildx build --platform linux/amd64 --load -t "${DOCKER_IMAGE}" -f api/Dockerfile .
else
  echo "No arm/aarch OS arch detected"
  docker build -t "${DOCKER_IMAGE}" -f api/Dockerfile .
fi

docker push "${DOCKER_IMAGE}"
